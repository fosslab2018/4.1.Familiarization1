administrator@administrator:~$ git config --global user.name "Abhishek Hari"
administrator@administrator:~$ git config --global user.email "abhishekhari455@gmail.com"
administrator@administrator:~$ git clone https://gitlab.com/fosslab2018/4.1.Familiarization1.git
Cloning into '4.1.Familiarization1'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
Checking connectivity... done.
administrator@administrator:~$ cd 4.1.Familiarization1
administrator@administrator:~/4.1.Familiarization1$ touch README.md
administrator@administrator:~/4.1.Familiarization1$ git add README.md
administrator@administrator:~/4.1.Familiarization1$ git commit -m "add README"
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean
administrator@administrator:~/4.1.Familiarization1$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS004
Password for 'https://ASI16CS004@gitlab.com': 
Branch master set up to track remote branch master from origin.
Everything up-to-date
administrator@administrator:~/4.1.Familiarization1$ touch edited_lab1.pdf
administrator@administrator:~/4.1.Familiarization1$ touch lab2.odt
administrator@administrator:~/4.1.Familiarization1$ git add edited_lab1.pdf
administrator@administrator:~/4.1.Familiarization1$ git add lab2.odt
administrator@administrator:~/4.1.Familiarization1$ git commit -m "files"
[master 79cec96] files
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 edited_lab1.pdf
 create mode 100644 lab2.odt
administrator@administrator:~/4.1.Familiarization1$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS004
Password for 'https://ASI16CS004@gitlab.com': 
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 1.93 MiB | 584.00 KiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
To https://gitlab.com/fosslab2018/4.1.Familiarization1.git
   493f22d..79cec96  master -> master
Branch master set up to track remote branch master from origin.
administrator@administrator:~/4.1.Familiarization1$ 
